#!/usr/bin/env node

const https = require('https');
const express = require('express');
const fs = require('fs')
const path = require('path');
const createError = require('http-errors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const methodOverride = require('method-override');
const compression = require('compression');
const axios = require('./utils/axios');
const auth = require("./auth.json")
const { authenticate, isModerator } = require("./utils/utils");

const DEFAULT_AUTH_JS =
{
  "username": "admin",
  "password": "test",
  "uploadToken": "meme",
  "webAPI": "http://localhost:8443/v1/",
  "NewsAdminIPs": ["localhost"]
};

// write out default config if not exist
if (!fs.existsSync("./auth.json")) {
  fs.writeFile("./auth.json", JSON.stringify(DEFAULT_AUTH_JS));
}

process.on('uncaughtException', function (error) {
  console.log(error.stack);
});

const app = express();

// Set up Handlebars
app.engine('hbs', require('./utils/helpers').engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(cookieParser());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'))
app.use(express.static(path.join(__dirname, 'public'), {maxAge: '1d'}));
app.use((req, res, next) => {
  axios.get(auth.webAPI + 'players/online-count')
    .then(response => {
      req.playerCount = response.data.count;
      next();
    })
    .catch(error => {
      console.error('API call error:', error);
      req.playerCount = "0";
      next();
    });
});


app.use('/', require('./routes/index'));
app.use('/news', require('./routes/articles'));
app.use('/users', require('./routes/users'));

app.get('/edit-news', async (req, res) => {
  if(isModerator(req)) {
    axios.get(auth.webAPI + "web?page=1&limit=99999&type=0")
        .then((response) => {
          res.render('articles/index', { layout: "layout-writenews", webAPI: auth.webAPI, articles: response["data"], isModerator: true });
        });
    return;
  }
  res.redirect('/');
});

app.post('/login', async (req, res) => {
  res.cookie("username", req.body.username)
  res.cookie("password", req.body.password)
  res.redirect('/edit-news');
});

app.get("/admin", (req, res) => {
  if(isModerator(req)) {
    res.redirect('/edit-news');
    return;
  }
  res.render('articles/auth', { layout: "layout-writenews" })
});

app.get('/logout', (req, res) => {
  res.cookie("username", "")
  res.cookie("password", "")
  res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err);

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.set('port', port);

const httpsServer = https.createServer({
  key: fs.readFileSync(auth.keyPath),
  cert: fs.readFileSync(auth.certPath)
}, app);

httpsServer.on('error', (error) => {
  console.log(error);
});

httpsServer.on('listening', () => {
  const addr = httpsServer.address();
  console.log('Listening ' + addr.address + ' : ' + addr.port);
});

var port = process.env.PORT || auth.port;
if (!port || isNaN(port) || port <= 0 || port > 65535) {
  console.log("Failed to detect port. Edit your auth.json or `export PORT=443`")
  return;
}

httpsServer.listen(port);
