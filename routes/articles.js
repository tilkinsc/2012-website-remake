const express = require('express')
const router = express.Router()
const { isModerator } = require("../utils/utils");
const {localAPI} = require("../utils/axios");
const axios = require('axios');
const auth = require("../auth.json")


router.get('/list', (req, res) => {
    res.redirect('list/0/1');
})

router.get('/list/:type/:page', async (req, res) => {
    localAPI.get(`/web?page=${req.params.page}&limit=6&type=${req.params.type}`)
        .then((response) => {
            res.render('pages/news-listing', { layout: "layout", page: req.params.page, type: req.params.type,
                webAPI: auth.webAPI, articles: response["data"], news: true, playerCount: req.playerCount  });
        });
})

router.get('/new', (req, res) => {
    if(isModerator(req))
        res.render('articles/new', { layout: "layout-writenews", webAPI: auth.webAPI, isModerator: isModerator(req) })
    else
        res.redirect('/');
})

router.get('/edit/:id', async (req, res) => {
    if(isModerator(req))
        localAPI.get(`/web/get-article/${req.params.id}`)
            .then((response) => {
                res.render('articles/edit', { layout: "layout-writenews", isModerator: isModerator(req),
                    webAPI: auth.webAPI, article: response["data"], editArticle: true });
            });
    else
        res.redirect('/');
})

router.post('/add', async (req,res) => {
    axios.post("https://localhost:8443/v1/web/create-article", req.body).then((response) => {
        ;
    }).catch((error) => {
        console.error(error);
    });
})

router.post('/edit/:id', async (req,res) => {
    axios.post("https://localhost:8443/v1/web/edit/" + req.params.id, req.body).then((response) => {
        ;
    }).catch((error) => {
        console.error(error);
    });
})

router.post('/delete/:id', async (req,res) => {
    axios.post("https://localhost:8443/v1/web/delete/" + req.params.id, req.body).then((response) => {
        ;
    }).catch((error) => {
        console.error(error);
    });
})

router.get('/:slug', async (req, res) => {
    localAPI.get(`/web/get-article-slug/${req.params.slug}`)
        .then((response) => {
            if (response['data'] == '') res.redirect('/')
            res.render('articles/show', { article: response["data"], isModerator: isModerator(req), layout: "layout", news: true, playerCount: req.playerCount  });
        });
})

module.exports = router
