let express = require('express');
const { localAPI } = require("../utils/axios");
const { getSkillIDByName, formatRSNickName, fetchSkills } = require("../utils/utils");

let router = express.Router();

// TODO: Blog Authentication
// TODO: 404 Page
// TODO: Player count on front page
// TODO: Fix clicking all players

/* GET home page. */
// https://web.archive.org/web/20120709052134/http://www.runescape.com:80/title.ws
router.get('/', async (req, res, next) => {
    localAPI.get("/web?page=1&limit=6&type=0")
        .then((response) => {
            res.render('index', { layouts: 'layout.hbs', articles: response['data'], playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120731001805/http://www.runescape.com:80/world_locations.ws
router.get('/world', function(req, res, next) {
  res.render('pages/world/world', { layout: 'layout.hbs', world: true, playerCount: req.playerCount  });
});

router.get('/rs-wiki', function(req, res, next) {
  res.redirect('https://runescape.wiki/', {});
});

// https://web.archive.org/web/20120718053538/http://www.runescape.com/downloads.ws
router.get('/downloads', function(req, res, next) {
  res.render('pages/downloads', { layout: 'layout.hbs', downloads: true, playerCount: req.playerCount  });
});

// https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
router.get('/hall-of-heroes', function(req, res, next) {
    localAPI.get("/highscores?page=1&limit=10")
        .then((response) => {
            res.render('pages/hall-of-heroes', { layout: 'layout.hbs', heroes: true, highscores: response, playerCount: req.playerCount  });
        });
});

// https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
router.get('/temporal-hall-of-heroes', function(req, res, next) {
    res.redirect("/temporal-hall-of-heroes/0/1");
});

// https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
router.get('/temporal-hall-of-heroes/:pastday/:secondpastday', function(req, res, next) {
    localAPI.get(`/temporal?page=1&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
        .then((response) => {
            res.render('pages/temporal-hall-of-heroes', { layout: 'layout.hbs', heroes: true, highscores: response,
                firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/highscores', function(req, res, next) {
    localAPI.get("/highscores?page=" + 1 +"&limit=" + 22)
        .then((response) => {
            res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: "Overall", page: 1,
                limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-highscores', function(req, res, next) {
    localAPI.get("/temporal?page=" + 1 +"&limit=" + 10)
        .then((response) => {
            res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: "Overall", page: 1,
                limit: 10, firstDayPast: 0, secondDayPast: 1, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-highscores/overall/:pastday/:secondpastday/:page', function(req, res, next) { 
    localAPI.get(`/temporal?page=${req.params.page}&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
        .then((response) => {
            res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: "Overall", page: req.params.page,
                limit: 10, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-highscores/:skill/:pastday/:secondpastday/:page/', function(req, res, next) {
    localAPI.get(`/temporal?page=${req.params.page}&skill=${getSkillIDByName(req.params.skill)}&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
        .then((response) => {
            res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: req.params.skill,
                page: req.params.page, limit: 10, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-highscores-player/:user', function(req, res, next) {
    localAPI.get(`/temporal/player?username=${req.params.user}`)
        .then((response) => {
            let username = formatRSNickName(req.params.user)
            response = response['data']
            res.render('pages/temporal-player-highscores', { layout: 'layout.hbs', displayName: username, overallLevelsUp: response.overallLevelsUp,
                totalXP: response.totalXp, skillXPs: response.xpDifferential, overallRank: response.overallRank, levelsUp: response.levelsUp, xpRanks: response.xpRanks,
                playerHighscore: true, firstDayPast: 0, secondDayPast: 1, playerCount: req.playerCount });
        })
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-highscores-player/:user/:pastday/:secondpastday', function(req, res, next) {
    localAPI.get(`/temporal/player?username=${req.params.user}&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
        .then((response) => {
            let username = formatRSNickName(req.params.user)
            response = response['data']
            res.render('pages/temporal-player-highscores', { layout: 'layout.hbs', displayName: username, overallLevelsUp: response.overallLevelsUp,
                totalXP: response.totalXp, skillXPs: response.xpDifferential, overallRank: response.overallRank, levelsUp: response.levelsUp, xpRanks: response.xpRanks,
                playerHighscore: true, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
        })
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/temporal-chart/:user/:pastday/:secondpastday', function(req, res, next) {
    localAPI.get(`/temporal/player?username=${req.params.user}&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
        .then((response) => {
            let username = formatRSNickName(req.params.user)
            response = response['data']
            res.render('pages/temporal-player-chart', { layout: 'layout.hbs', displayName: username, overallLevelsUp: response.overallLevelsUp,
                totalXP: response.totalXp, skillXPs: response.xpDifferential, overallRank: response.overallRank, levelsUp: response.levelsUp, xpRanks: response.xpRanks,
                playerHighscore: true, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
        })
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/grandexchange', function(req, res, next) {
    res.redirect('grandexchange/buy/1');
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/grandexchange/buy/:page', function(req, res, next) {
    localAPI.get("/ge/buy?page=" + req.params.page +"&limit=" + 22)
        .then((response) => {
            filteredList = { "data": [] }
            for(let i = 0; i < response['data'].length; i++)
                if(response['data'][i].state != 'FINISHED')
                    filteredList["data"].push(response['data'][i])
            console.log(response)
            res.render('pages/grandexchange', { layout: 'layout.hbs', highscore: true, itemListing: response, type: "buy", page: req.params.page,
                limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/grandexchange/sell/:page', function(req, res, next) {
    localAPI.get("/ge/sell?page=" + req.params.page +"&limit=" + 22)
        .then((response) => {
            filteredList = { "data": [] }
            for(let i = 0; i < response['data'].length; i++)
                if(response['data'][i].state != 'FINISHED')
                    filteredList["data"].push(response['data'][i])
            res.render('pages/grandexchange', { layout: 'layout.hbs', highscore: true, itemListing: filteredList, type: "sell", page: req.params.page,
                limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/highscores/Overall/:page', function(req, res, next) {
    localAPI.get("/highscores?page=" + req.params.page +"&limit=" + 22)
        .then((response) => {
            res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: "Overall", page: req.params.page,
                limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/highscores/:skill/:page', function(req, res, next) {
    localAPI.get("/highscores?skill=" + getSkillIDByName(req.params.skill) + "&page=" + req.params.page +"&limit=" + 22)
        .then((response) => {
          res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: req.params.skill, page: req.params.page,
          limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/highscores/:skill', function(req, res, next) {
    localAPI.get("/highscores?skill=" + getSkillIDByName(req.params.skill) + "&page=" + 1 +"&limit=" + 22)
        .then((response) => {
            res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: response, skill: req.params.skill, page: 1,
                limit: 22, playerCount: req.playerCount });
        });
});

// https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
router.get('/highscores-player/:user', function(req, res, next) {
    localAPI.get(`/highscores?limit=9999999`)
        .then((response) => {
            let username = formatRSNickName(req.params.user)
            let skillsData = fetchSkills(response, username);
            res.render('pages/player-highscores', { layout: 'layout.hbs',
                displayName: username, totalLevel: skillsData['totalLevel'], totalXP: skillsData['totalXP'],
                totalRank: skillsData['totalRank'], skillXPs: skillsData['skillXPs'],
                skillRanks: skillsData['skillRanks'], playerHighscore: true, playerCount: req.playerCount  });
        })
});

module.exports = router;
